package lsl;

import com.lsl.service.ServiceInterface;

import java.util.ServiceLoader;

/**
 * @Auther LSL
 * @Date 2022/2/23
 */
public class App {
    public static void main(String[] args) {
        ServiceLoader<ServiceInterface> interfaces = ServiceLoader.load(ServiceInterface.class);
        for (ServiceInterface impl : interfaces) {
            impl.test();
        }
    }
}
